<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = DB::table('users')->insert([
            'name' => 'Toavina',
            'email' => 'mendrikatoavina12@gmail.com',
            'password' => Hash::make('toavina'),
            'role' => 'admin',
        ]);

        $user = DB::table('users')->insert([
            'name' => 'John',
            'email' => 'johnDoe@gmail.com',
            'password' => Hash::make('toavina'),
            'role' => 'user',
        ]);
    }
}
