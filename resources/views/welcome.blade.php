@extends('layouts.app')

@section('content')


  <div class="section no-pad-bot" id="index-banner">
      <div class="container">
        <br><br>
        <h1 class="header center orange-text">Board cyber sécurité</h1>
        <div class="row center">
          <h5 class="header col s12 light">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores a neque, mollitia cupiditate ex inventore maxime provident ipsum ipsam, aspernatur </h5>
        </div>
        <div class="row center">
          <a href="{{ route('posts.index')}}" id="download-button" class="btn-large waves-effect waves-light orange">Signaler</a>
        </div>
        <br><br>

      </div>
    </div>

    <div class="container">
        <div class="row">
            @foreach ($posts as $post)
                <div class="col s12 m6">
                    <div class="card blue-grey darken-1">
                      <div class="card-content white-text">
                        <span class="card-title">{{ $post->title }}</span>
                      </div>
                      <div class="card-action">
                        <a href="{{ route('posts.show', $post)}}">Voir</a>
                        @auth
                            @can('update', $post)
                                <a href="{{ route('posts.edit', $post)}}">Editer</a>
                            @endcan
                            @can('delete', $post)
                                <a href="{{  route('posts.destroy',$post) }}"
                                    onclick="event.preventDefault();
                                                  document.getElementById('destroy-form').submit();">
                                    Supprimer
                                </a>
                                <form id="destroy-form" action="{{ route('posts.destroy',$post) }}" method="post">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                </form>
                            @endcan 
                        @endauth
                      </div>
                    </div>
                </div>  
            @endforeach
         
        </div>
    
    </div>


    
@endsection