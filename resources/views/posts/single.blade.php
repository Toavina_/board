@extends('layouts.app')

@section('content')
    
    <div class="container">
        <h1>{{ $post->title }}</h1>
        <p>{!! $post->content !!}</p>
        <div class="col-md-8">
            @guest
            <div class="card">
                <div class="card-header">Commenter</div>

                <div class="card-body">
                <p class="text-center"><a href="{{ route('login') }}">Connecter</a> vous ou <a href="{{ route('register') }}">crée un compte</a> pour commenter </p>                        
                </div>
            </div>   
            @else
            <div class="card">
                <div class="card-header">Ajouter un commentaire</div>

                <div class="card-body">
                    <form method="post" action="{{ route('posts.store') }}">
                        @csrf
                        <div class="form-group">
                          <textarea class="form-control" name="content" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Commenter</button>
                        </div>
                    </form>
                        
                </div>
            </div>   
            @endguest
        </div>
    </div>
@endsection