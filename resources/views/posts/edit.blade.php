@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="text-center">Modification</h1>
        <div class="row ">
            <div class="col s12">
                <div class="card" style="padding:10px">
                    <h3>Modifier</h3>
                    @if(Session::has('add_post_success'))
                        <div class="alert alert-success">
                            {{ Session::get('add_post_success') }}
                        </div>
                    @endif
                   
                        <form method="POST" action="{{ route('posts.update',$post) }}">
                            @csrf
                            <input name="_method" type="hidden" value="PUT">
                            <div class="row">
                                <div class="input-field col s12">
                                    <label for="title">Titre</label>
                                  <input type="text" name="title" id="title" placeholder="titre" value="{{ $post->title }}">
                                      @if ($errors->has('title'))
                                          <span class="invalid-feedback" role="alert">
                                              <strong>{{ $errors->first('title') }}</strong>
                                          </span>
                                      @endif
                                  </div>

                                </div>
                                <div class="row">

                                    <div class="input-field col s12">
    
                                        <div class="form-group">
                                          <label for="editor">Contenue</label>
                                          <textarea name="content" id="editor" rows="3">{{ $post->content }}</textarea>
                                          @if ($errors->has('content'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('content') }}</strong>
                                            </span>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                            <div class="row">
                                <div class="input-field col s12">
                                        <button type="submit" class="btn btn-primary">Envoyer</button>
                                </div>
                                
                            </div>
                         
                        </form>
                            
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection