@extends('layouts.app')

@section('content')
    <div class="container">
        <h2 class="text-center">Tous mes Publications</h2>
        <div class="row">
            <div class="col s12 l8">
                <div class="card" style="padding:10px">
                        @if(Session::has('add_post_success'))
                        <div class="card-panel teal lighten-2"> {{ Session::get('add_post_success') }}</div>
                        @endif
                        <form method="post" action="{{ route('posts.store')}}">
                            @csrf
                                <h4>Publier</h4>
                    
                            <div class="row">
                                <div class="input-field col s12">   
                                    <input type="text" id="email" class="validate" name="title" value="{{ old('title') }}" required/>
                                    <label for="email">Titre</label>
                                </div>
                            </div>
                            <div class="row">
                                <label for="edior">Description</label>
                                <div class="input-field col s12">   
                                    <textarea name="content" id="editor" cols="30" rows="50"></textarea>
                                    
                                </div>
                            </div>
                            <div class="row center-align">
                                    <button class="btn waves-effect waves-light" type="submit" name="action">Publier
                                        <i class="material-icons right">send</i>
                                    </button>
                            </div>
                        </form>
                     
                </div>
            </div>
        </div>

        <div class="row">
                @foreach ($posts as $post)
                    <div class="col s12 m6">
                        <div class="card blue-grey darken-1">
                          <div class="card-content white-text">
                            <span class="card-title">{{ $post->title }}</span>
                          </div>
                          <div class="card-action">
                            <a href="{{ route('posts.show', $post)}}">Voir</a>
                            @auth
                                @can('update', $post)
                                    <a href="{{ route('posts.edit', $post)}}">Editer</a>
                                @endcan
                                @can('delete', $post)
                                    <a href="{{  route('posts.destroy',$post) }}"
                                        onclick="event.preventDefault();
                                                      document.getElementById('destroy-form').submit();">
                                        Supprimer
                                    </a>
                                    <form id="destroy-form" action="{{ route('posts.destroy',$post) }}" method="post">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                    </form>
                                @endcan 
                            @endauth
                          </div>
                        </div>
                    </div>  
                @endforeach
             
            </div>
        </div>
    </div>
@endsection