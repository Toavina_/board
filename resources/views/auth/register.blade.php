@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col s12">
            <div class="card grey lighten-3">
                <div class="card-content">
                    <h4 class="card-title center-align">{{ __('lang.register') }}</h4>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="row">
                            <div class="input-field col s12">   
                                <input type="text" id="nom" class="validate" name="name" value="{{ old('name') }}" required autofocus />
                                <label for="nom">Nom</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input type="email" id="email" class="validate" name="email" value="{{ old('email') }}" required/>
                                <label for="email">{{ __('lang.email') }}</label>
                            </div>
                        </div>
                        <div class="row">
                                <div class="input-field col s12">
                                    <input type="password" id="password" class="validate" name="password" value="{{ old('email') }}" required/>
                                    <label for="password">{{ __('lang.password') }}</label>
                                </div>
                        </div>
                        <div class="row">
                                <div class="input-field col s12">
                                    <input type="password" id="password-confirm" class="validate" name="password-confirm" value="{{ old('email') }}" required/>
                                    <label for="password-confirm">{{ __('lang.password') }}</label>
                                </div>
                        </div>
                        <div class="row center-align">
                            <button class="btn waves-effect waves-light" type="submit" name="action">{{ __('lang.register') }}
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
