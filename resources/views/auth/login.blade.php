@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col s12 l4 offset-l4">
            <div class="card grey lighten-3">
                <div class="card-content">
                    <h4 class="card-title center-align">{{ __('lang.login') }}</h4>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix">email</i>
                                <input type="email" id="email" class="validate" name="email" required/>
                                <label for="email">{{ __('lang.email') }}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix">vpn_key</i>
                                <input type="password" id="password" class="validate" name="password" required/>
                                <label for="password">{{ __('lang.password') }}</label>
                            </div>
                        </div>
                        <div class="row center-align">
                            <button class="btn waves-effect waves-light" type="submit" name="action">Envoyer
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                        <div class="row center-align">
                            <label>
                                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} />
                                <span>{{ __('lang.remember_my') }}</span>
                            </label>
                            <br>
                            <a href="{{ route('password.request') }}">
                                {{ __('lang.forgot_password') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
