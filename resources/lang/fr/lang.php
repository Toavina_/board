<?php
return [
   
    'email'         => 'Addresse email',
    'password'      => 'Mot de passe',
    'remember_my'   => 'Se souvenir de moi',
    'login'         => 'S\'authentifier',
    'register'      => 'S\'inscrire',
    'forgot_password'      => 'Mot de passe oublier',
];