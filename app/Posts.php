<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $fillable = ['title','content','user_id'];

    public function user() {
        return $this->belongsTo(User::class);
    }
    /**
     * Faire des requête complexe dans une methode
     *
     * @param [type] $query
     * @param [type] $user
     * @return void
     */
    public function scopeUserPost($query,$user)
    {
        return $query->where('user_id',$user);
    }
    /**
     * Getter & Setter Mutateur
     * getNomduchampAttribute()
     * @param [type] $value
     * @return void
     */
    public function getTitleAttribute($value)
    {
        return strtoupper($value);
    }
}
