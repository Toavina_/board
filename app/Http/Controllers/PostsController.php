<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;
use Illuminate\Support\Facades\Auth;
use App\Posts;
use Symfony\Component\HttpFoundation\Session\Session;
use Illuminate\Support\Facades\Gate;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        if(Auth::check()){
            $posts = Posts::userPost(Auth::id())->get();
            return view('posts.index', compact('posts'));
        }
        return redirect(route('login'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::check()) {
            $this->validate($request, [
                'title' => 'required',
                'content' => 'required'
            ]);
            $post = $request->all();            
            $post = array_merge($post,['user_id' => Auth::id()]);
            Posts::create($post);
            $request->session()->flash('add_post_success', 'Votre article est publié');
            return redirect(route('posts.index'));
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Posts::find($id);
        $this->authorize('view',$post);
        return view('posts.single', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::check()){
            $post = Posts::findOrFail($id);
            $this->authorize('update',$post);
           
            return view('posts.edit', compact('post'));
          
                       
        } 
        return redirect()->back();    
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::check()){
            
           $post = Posts::findOrFail($id);
           $this->authorize('update',$post);
           $post->update($request->all());
           return redirect(route('welcome'));
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::check()){
            $post = Posts::findOrFail($id);
            $this->authorize('delete',$post);
            Posts::where('id',$id)->delete();
            return redirect()->back();    
        }
    }
}
