<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;

class WelcomeController extends Controller
{
    public function index()
    {
        $posts = Posts::get();
        return view('welcome', compact('posts'));
    }
}
