
<h1 align="center">Board</h1>
<p  align="center"> <img  src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p  align="center">
<a  href="https://travis-ci.org/laravel/framework"><img  src="https://travis-ci.org/laravel/framework.svg"  alt="Build Status"></a>
<a  href="https://packagist.org/packages/laravel/framework"><img  src="https://poser.pugx.org/laravel/framework/d/total.svg"  alt="Total Downloads"></a>
<a  href="https://packagist.org/packages/laravel/framework"><img  src="https://poser.pugx.org/laravel/framework/v/stable.svg"  alt="Latest Stable Version"></a>
<a  href="https://packagist.org/packages/laravel/framework"><img  src="https://poser.pugx.org/laravel/framework/license.svg"  alt="License"></a>
</p>

## Server Requirments

- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension



## Installation de l'application

Cloner le dêpot

  

	git clone ssh://git@bitbucket.esti.mg/kdkbzdnl/board.git
 1. Creer la base de donnée

 2. Creer votre fichier .env en copiant .env.example
 3. Installer les dépendances

 `composer install`

 4. Générer la cléf d'application

`php artisan key:generate`

 

 5. Configurer la connection à la base de donnée en modifiant le .env

``` 
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=<nom  base  de  donnée>
DB_USERNAME=<nom  utilisateur>
DB_PASSWORD=<mot  de  passe  base  de  donnée> 
```

 6. Creer les tables avec les migrations avec un utilisateur simple et un utilisateur admin

`php artisan migrate --seed`

| role | email | mot de passe|
|------|-------|-------------|
| Admin | mendrikatoavina12@gmail.com | toavina |
| simple | johnDoe@gmail.com | toavina |

7. Ouvrir l'application avec un serveur

## Développement
L'application utilise webpack qui intégrer par défaut à Laravel

 - Installer les dépendances avec: 

`npm i`

 - Compiler les assets avec un script qui recompile à chaque modification

`npm run watch`

 - Compiler et minifier les assets pour l'envirenement de production

 `npm run prod`

## Les commits
Les conventions pour faire les commits

|  prefix    |  action |
|------------|------------|
| `feat`     |  pour les commits qui rajoute des fonctionnalités à l'appication  |
| `view`     | pour les commits qui concerne les vues                            |
| `fix`      | pour les commits qui corrige des bugs                             |
| `refactor` | pour le "refactoring" du code                                     |
| `doc`      | pour la modification de la documentation                          | 

